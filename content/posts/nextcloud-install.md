---
title: "How easy is to setup a Nextcloud instance in 2021"
date: 2021-12-03T21:37:25Z
draft: false
tags: [Nexcloud, Ubuntu]
---
In this post, I will show to set up a Nextcloud instance and take it to production. To start will look into it by enabling the firewall, configuring it then installing Nextcloud and also configuring it.

## Configure firewall

Write these rules and after enabling it with `sudo ufw enable`

```bash
# Open SSH port
ufw allow 22/tcp

#Open HTTP and HTTPS ports
ufw allow 80,443/tcp



# Open port 81 for Let's Encrypt (Get the HTTPS certificate)
ufw allow 81/tcp
```


## Installing Nextcloud

Installing Nextcloud with Snap is just running in the terminal `sudo snap install nextcloud`.

Then you want to run this command `sudo nextcloud.manual-install name password` providing the username and password you want for the admin user.

Next, you need to allow your domain to access nextcloud instance. To allow it use this `sudo nextcloud.occ config:system:set truested_domains 1 --value=yourdomain.com`.

To finish all you have to do is `sudo nextcloud.enable-https` and enter your email and domain name to get the SSL certificate. 

There you go, now you can access your Nextcloud instance in the browser.