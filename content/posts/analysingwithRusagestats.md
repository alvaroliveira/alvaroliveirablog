---
title: "Analysing phone usage data with R and SQL"
date: 2022-06-04T12:55:08+01:00
draft: false
---


In this blog post, I'll show how I used R and SQL to do a phone usage data report.
The app that I use is still in beta so there aren't have graph or data other than usage by day, so I decided to do it myself for a nice challenge.
The end result is here, although I did one more graph, its not very interesting, so the main ones are here but I not show the Y axis because I dont want to share that information in public.



![MeanUsageApps](/meanTop3SocialMedia.png)

![UsageByDay](/UsageByDay.png)

![UsageByDayBoxPlot](/UsageByDayBoxPlot.png)


For this script to work you'll need to install two libraries `ggplot2` and `RSQLite`.

Although I used SQL for data querying and R for data visualization, I could have used R for both data querying and data visualization with the `RSQLite.dbReadTable` method and then do the calculations in R but I find it very easy to do the stuff I did in SQL.


```r
library(DBI)
library(ggplot2)


con <- dbConnect(RSQLite::SQLite(),
                 "pathToDataExportBy\\usageDirect-history.sqlite3")


sumTotalMinutesByDay <- dbGetQuery(con, "Select day, sum((timeUsed / (1000 * 60)) % 60) as minutes
                                        FROM usageStats
                                        GROUP BY day")

usageSocialMedia <- dbGetQuery(con, "SELECT applicationId, avg(timeUsed/(1000 * 60) % 60) as timeInMinutes 
                                    FROM usageStats
                                    WHERE applicationId = 'com.instagram.android' 
                                        OR applicationId = 'com.twitter.android' 
                                        OR applicationId = 'com.google.android.youtube'
                                    GROUP BY applicationId")

maxHoursUsageSocialMedia <- dbGetQuery(con, "SELECT applicationId, max(timeUsed / (1000 * 60 * 60)) % 24 as hours 
                                            FROM usageStats
                                            WHERE applicationId = 'com.instagram.android' 
                                                OR applicationId = 'com.twitter.android' 
                                                OR applicationId = 'com.google.android.youtube' 
                                            GROUP by applicationId")

dbDisconnect(con)


meanTop3SocialMediaGraph <- ggplot(usageSocialMedia, aes(x=applicationId, y=timeInMinutes)) + ggtitle("Mean usage of Top 3 Social Media Apps") + xlab("Application")+ ylab("Minutes Used") + geom_bar(stat = "identity")

maxHoursTop3SocialMediaGraph <- ggplot(maxHoursUsageSocialMedia, aes(x=applicationId, y=hours)) + ggtitle("Max usage of Top 3 Social Media Apps") + xlab("Application") + ylab("Hours Used") + geom_bar(stat="identity")

sumTotalMinutesByDayGraph <- ggplot(sumTotalMinutesByDay, aes(x=day, y=minutes))  + ggtitle("Usage by day")  + xlab("Day")  + ylab("Minutes Used")   + geom_line()  + geom_point()

sumTotalMinutesByDayBoxPlot <- ggplot(sumTotalMinutesByDay, aes(x=day, y=minutes))  + ggtitle("BoxPlot usage by day")  + xlab("Day") + ylab("Minutes Used")  + geom_boxplot()

pdf(file = "pathTo\\usageStatsReport.pdf")

print(meanTop3SocialMediaGraph)
print(maxHoursTop3SocialMediaGraph)
print(sumTotalMinutesByDayGraph)
print(sumTotalMinutesByDayBoxPlot)

dev.off()	
```
