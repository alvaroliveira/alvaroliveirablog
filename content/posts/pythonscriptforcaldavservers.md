---
title: "Building a Python Script for listing events from a CalDav Server in the terminal"
date: 2022-05-31T21:56:57+01:00
draft: false
tags : [programming, python, terminal]
---

I wanted a very simple form for viewing my events and taskd, I needed a solution to be very fast, so I decided to build a script to access the events on my Nextcloud instance on a terminal. I used Python for this because it has some great libraries not only for accessing CalDav server but also for printing things on the screen.


This is the end result. It displays event from a week before today and a week after today.

![TUICal](/printcal.jpeg)


The source code is here, its also a very straight forward program. One important thing to notice is the fact the script reads the credentials from a json file.


```python
import caldav
from datetime import datetime, date, timedelta
from operator import attrgetter
from rich.console import Console
from rich.table import Table
from rich import print
from rich.layout import Layout

import json

jsonFile = open('configCal.json', 'r')
config = json.load(jsonFile)
jsonFile.close()

caldav_url = config['url']
username = config['username']
password = config['password']
calendarName = config['calendar']
tasksName = config['tasks']


client = caldav.DAVClient(url=caldav_url, username=username, password=password)
my_principal = client.principal()
calendars = my_principal.calendars()

calendar = my_principal.calendar(name=calendarName)


events_fetched = calendar.date_search(
    start=(datetime.today() - timedelta(days=7)),
    end=(datetime.today() + timedelta(days=7)),
    expand=True)
sorted_events = sorted(events_fetched, key=attrgetter('vobject_instance.vevent.dtstart.value'))


tasksCalendar = my_principal.calendar(name=tasksName)
tasks = tasksCalendar.todos(sort_key='DTSTAMP')

client.close()


events_table = Table(title='Events')

events_table.add_column('Name ✒')
events_table.add_column('Date 📆')
events_table.add_column('Hours ⌚')

for event in sorted_events:
    events_table.add_row(
            event.vobject_instance.vevent.summary.value,
            str(event.vobject_instance.vevent.dtstart.value.date()),
            "{0}:{1} - {2}:{3}".format(str(event.vobject_instance.vevent.dtstart.value.hour),
            str(event.vobject_instance.vevent.dtstart.value.minute),str(event.vobject_instance.vevent.dtend.value.hour),
            str(event.vobject_instance.vevent.dtend.value.minute)))
            

console = Console()

console.rule(f'[bold deep_sky_blue1]Viewing events for: {calendarName}')

tasks_table = Table(title='Tasks')

tasks_table.add_column('Task')
tasks_table.add_column('Date')

for task in tasks:
    tasks_table.add_row(task.vobject_instance.vtodo.summary.value, str(task.vobject_instance.vtodo.dtstamp.value.date()))


layout = Layout()

layout.split_row(
    Layout(name='events'),
    Layout(name='tasks')
)


layout['events'].split(
    Layout(events_table)
)

layout['tasks'].split(
    Layout(tasks_table)
)

print(layout)
``` 





