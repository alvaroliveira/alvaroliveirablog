---
title: "Hacking Owasp Juice Web Shop"
date: 2022-06-08T21:38:01+01:00
draft: false
---

In this blog post I will try to show how to solve all challenges of OWASP Juice Web Shop. This is a work in progress.

### SQL Injection in the admin login

This one is easy, it is a simple sql injection that lets you bypass admin login.

Just type `"OR 1==1 --` in the admin email input and type something random in the password.

This works because `1==1` returns always true and `OR` operator is true if one of the conditions is true so it validates the query. Also `--` comments out the rest of the SQL Query so it doesn't matter what you put in the password.


### Error Handling

While trying to get the SQL Injection, I got the error handling challenge right because the web app doesnt handle right errros and spits errors from the backend.


### DOM XSS 

In the search bar there is a XSS vulnerability. After trying `<p>Hello</p>` it is possible to see the html is being inserted into to the page, so next logical step was to try `<script> alert(1)</script>` but it didn't triger an alert box. 
After many tries and some research I did manage to get and alert with this payload `<a onmouseover="alert(1)"\>Click me!</a>`.


### Score Board

The score board can be found by trying to brute force the url. First tried all combinations of `/scoreboard` but then tried `/score-board` and it works.
