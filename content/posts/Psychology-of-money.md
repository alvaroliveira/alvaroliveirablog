---
title: "Main takeaways of Psychology of Money by Morgan Housel"
date: 2021-12-04T20:01:31Z
draft: true
tags: [Books, Money]
---

In his book [Psycology of Money: Timeless lessons on wealth, greed, and happiness](https://www.goodreads.com/book/show/41881472-the-psychology-of-money), Morgan Housel has some interesting points about how to handle money and financial situations.

The first idea is that financial success is not hard science but a soft skill, where how you behave is more important than what you know. This idea is interesting because when you think about financial you probably think about formulas and math but in reality, finance is a much more complicated area that involves humans so involves psychology and behaviorism.

The second is that you don’t need a specific reason to save and you should save for the sake of saving because it helps create what he calls Room for error (and is also called Margin of safety) which can be a frugal budget, savings, and long timeline. It is something that provides you with some air to breathe in terms of money.

The third is about greed and he has some points like *Less ego, more wealth* and *Enough is not too little*. He says that “Saving money is the gap between your ego and your income” which is interesting because often people buy a lot of stuff to “compete” with their peers. Another point is that you should manage your money in a way that you never trade a night of sleep by risking a lot, this is true for spending and investing.

The last ones are about investing. In investing you should define the game that you are playing because studying a specific person can be dangerous because we tend to study extreme examples or massive failures and the least applicable to our situations. Also by defining our game we are not influenced by other people's actions because they are playing a different game. Focus less on specific individuals and case studies and study more broad patterns because they will be more applicable to you. Luck and risk are the only outcomes in life guided by forces other than effort. Also, he talks about avoiding extreme ends of financial decisions because everyone’s goals and desires will inevitably change and the more extreme your past decisions were the more you may regret it as you evolve.

Down below you can find a mind map with ideas and quotes from the book.


![MindMapPsychologyOfMoney](/MindMapPsychology.png)

