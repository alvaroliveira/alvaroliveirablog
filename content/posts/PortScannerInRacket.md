---
title: "Making a Port Scanner In Racket"
date: 2022-05-28T19:23:10+01:00
draft: false
tags: [programming, racket]
---

In this blog post I will walktrough a Port Scanner written in Racket by me. If you want to see Port Scanner all together skip to end of the post.

## Language Definition and Modules

To start we need to first define the language to be interpreted and import the modules we will be using.

```racket
#lang racket

(require racket/tcp)
(require try-catch)

```

Racket comes with a module that lets connect with tcp interfaces like ports and create tcp servers. In this case its for connecting to tcp ports.
The second module was created by David K. Storrs and its for dealing with exceptions. It basically provides syntactic sugar for exceptions. 


## Main function

This function right here is the bulk of the program. It trys to connect to port in the host provied, if `tcp-connnect` cannot connect, raises and an exception `exn:fail:network?` and prints Port \<Port Number\> is closed. If it connects with success prints Port \<Port Number\> is open. One important thing is if it connects `tcp-connect` opens to buffers, one for input and the other for writing in the buffer to the port.

```racket
(define (port-open host port)
  (try [(tcp-connect host port)
      (printf "Port ~v is open\n" port)]
     [catch (exn:fail:network?
             (printf "Port ~v is closed\n" port))]))

```


## "Looping" through ports

This is a very simple function that uses recursion to go through all ports in the range. This function is written in a very functional way.

```racket
(define (scanner host port maxPort)
  (if (> port maxPort)
      'Done
      (begin (port-open host port)
             (scanner host (+ port 1) maxPort))))

```


## Command line arguments

`current-command-line-arguments` lets the programmer read the arguments user passes when calling the program. It returns a vector so to get the elements I converted to a list.

The program must have 3 parameters, one for the host ip, the lower bound port and the upper bound port. After converting the vector to a list its just navigating the list with `car's` and `cdr's`. The order of the arguments is Host next Lower bound then Upper bound.


```racket
(define arguments (vector->list (current-command-line-arguments)))

(define host (car arguments))
(define first (string->number(car (cdr arguments))))
(define last (string->number (car (cdr (cdr arguments)))))

```

## Grand Finale

After getting the arguments from the command line and defining the functions its pretty easy, just this single line.

```racket
(scanner host first last)
```

### Calling from the command line

To run this in the command line is only: 
```bash 
racket portScanner.rkt 127.0.0.1 1 1024
```


## Entire script
```racket
#lang racket

(require racket/tcp)
(require try-catch)

(define (port-open host port)
  (try [(tcp-connect host port)
      (printf "Port ~v is open\n" port)]
     [catch (exn:fail:network?
             (printf "Port ~v is closed\n" port))]))

(define (scanner host port maxPort)
  (if (> port maxPort)
      'Done
      (begin (port-open host port)
             (scanner host (+ port 1) maxPort))))

(define arguments (vector->list (current-command-line-arguments)))

(define host (car arguments))
(define first (string->number(car (cdr arguments))))
(define last (string->number (car (cdr (cdr arguments)))))

(scanner host first last)

```
